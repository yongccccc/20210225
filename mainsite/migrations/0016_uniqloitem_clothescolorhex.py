# Generated by Django 2.1.5 on 2021-01-28 16:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainsite', '0015_uniqloitem_clothescolor'),
    ]

    operations = [
        migrations.AddField(
            model_name='uniqloitem',
            name='ClothesColorHex',
            field=models.CharField(default='#D5E2D7', max_length=8),
        ),
    ]
